package main

import (
	"fmt"

	"gitlab.com/edoboker/gsr/src/gsr"
)

func main() {
	err := gsr.CreateRoute("172.16.1.1", "32", "192.168.216.1", false)
	if err != nil {
		fmt.Println(err)
	}
	err = gsr.DeleteRoute("172.16.1.1", "32", "192.168.216.1", false)
	if err != nil {
		fmt.Println(err)
	}

	gsr.InitServer()
}
