package gsr

import (
	"errors"
	"net/http"
	"strconv"
)

func routeHandler(w http.ResponseWriter, req *http.Request) {
	// for name, headers := range req.Header {
	// 	for _, h := range headers {
	// 		fmt.Fprintf(w, "%v: %v\n", name, h)
	// 	}
	// }

	switch req.Method {
	case "GET":
		prefix, mask, err := parseRouteShowVars(req)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			w.WriteHeader(200)
			route, routeErr := ShowRoute(prefix, mask)
			if routeErr != nil {
				http.Error(w, routeErr.Error(), http.StatusInternalServerError)
			} else {
				w.Write([]byte(route))
			}
		}
	case "POST":
		prefix, mask, nextHop, isDefault, err := parseRouteOperVars(req)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			w.WriteHeader(200)
			CreateRoute(prefix, mask, nextHop, isDefault)
		}
	case "DELETE":
		prefix, mask, nextHop, isDefault, err := parseRouteOperVars(req)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			w.WriteHeader(200)
			DeleteRoute(prefix, mask, nextHop, isDefault)
		}
	default:
		w.Write([]byte("Default"))
	}
}

func parseRouteShowVars(req *http.Request) (string, string, error) {
	if _, ok := req.Header["Prefix"]; !ok {
		return "", "", errors.New("prefix header not present")
	}

	if _, ok := req.Header["Mask"]; !ok {
		return "", "", errors.New("mask header not present")
	}

	prefix := req.Header["Prefix"][0]
	mask := req.Header["Mask"][0]

	return prefix, mask, nil
}

func parseRouteOperVars(req *http.Request) (string, string, string, bool, error) {
	if _, ok := req.Header["Prefix"]; !ok {
		return "", "", "", false, errors.New("prefix header not present")
	}

	if _, ok := req.Header["Mask"]; !ok {
		return "", "", "", false, errors.New("mask header not present")
	}

	if _, ok := req.Header["Nexthop"]; !ok {
		return "", "", "", false, errors.New("nextHop header not present")
	}

	if _, ok := req.Header["Isdefault"]; !ok {
		return "", "", "", false, errors.New("isDefault header not present")
	}

	prefix := req.Header["Prefix"][0]
	mask := req.Header["Mask"][0]
	nextHop := req.Header["Nexthop"][0]
	isDefault, _ := strconv.ParseBool(req.Header["Isdefault"][0])

	return prefix, mask, nextHop, isDefault, nil
}

//InitServer runs the http server listen proces and initializes the handlers
func InitServer() error {
	http.HandleFunc("/route", routeHandler)

	http.ListenAndServe(":8080", nil)

	return nil
}
