package gsr

import (
	"errors"
	"net"
	"os/exec"
	"runtime"
	"strconv"
)

//CreateRoute adds operating system level route
func CreateRoute(prefix string, mask string, nextHop string, isDefault bool) error {
	return osRouteOperation("add", prefix, mask, nextHop, isDefault)
}

//DeleteRoute deletes operating system level route
func DeleteRoute(prefix string, mask string, nextHop string, isDefault bool) error {
	return osRouteOperation("delete", prefix, mask, nextHop, isDefault)
}

//ShowRoute displays information on a route
func ShowRoute(prefix string, mask string) (string, error) {
	//Runtime verification
	osErr := verifyRuntime()
	if osErr != nil {
		return "", osErr
	}

	//Content validation
	if validateIpv4(prefix) == false {
		return "", errors.New("Invalid IP address in prefix")
	}

	_, maskConvertErr := strconv.Atoi(mask)
	if maskConvertErr != nil {
		return "", errors.New("Mask is not a valid integer")
	}

	//Running the command
	ipCmd, routeCmd := "ip", "route"
	operation := "show"
	prefixMaskArg := prefix + "/" + mask

	//command := strings.Join([]string{"sudo", ipCmd, routeCmd, operation, prefixMaskArg, "via", nextHop}, " ")
	//fmt.Println(command)
	output, err := exec.Command(ipCmd, routeCmd, operation, prefixMaskArg).Output()
	if err != nil {
		return "", err
	}
	return string(output), nil
}

func osRouteOperation(operation string, prefix string, mask string, nextHop string, isDefault bool) error {
	//Runtime verification
	osErr := verifyRuntime()
	if osErr != nil {
		return osErr
	}

	//Content validation
	if validateIpv4(prefix) == false && isDefault == false {
		return errors.New("Invalid IP address in prefix")
	}

	if validateIpv4(nextHop) == false {
		return errors.New("Invalid IP address in next hop")
	}

	_, maskConvertErr := strconv.Atoi(mask)
	if maskConvertErr != nil {
		return errors.New("Mask is not a valid integer")
	}

	//Running the command
	ipCmd, routeCmd := "ip", "route"
	var prefixMaskArg string
	if true == isDefault {
		prefixMaskArg = "default"
	} else {
		prefixMaskArg = prefix + "/" + mask
	}

	//command := strings.Join([]string{"sudo", ipCmd, routeCmd, operation, prefixMaskArg, "via", nextHop}, " ")
	//fmt.Println(command)
	_, err := exec.Command("sudo", ipCmd, routeCmd, operation, prefixMaskArg, "via", nextHop).Output()
	if err != nil {
		return err
	}
	return nil
}

func validateIpv4(addr string) bool {
	if net.ParseIP(addr) == nil {
		return false
	}
	return true
}

func verifyRuntime() error {
	if runtime.GOOS != "linux" {
		return errors.New("Operating system is not linux")
	}
	return nil
}
