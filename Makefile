build:
	mkdir -p bin
	go build -o ./bin/gsr ./src/main/*.go
	chmod +x ./bin/gsr

run:
	go run ./src/main/*.go